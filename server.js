const express = require('express')
  , sequelize = require('./configs/sequelize.config')
  , bodyParser = require("body-parser")
  , routes = require('./routes')
  , passportStrategy = require('./configs/passport-strategy.config')
  , expressValidator = require('express-validator')
  , appConfig = require('./services/app-config.services')
  , logger = require('./configs/logger.config');

const app = express();
passportStrategy();

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, Authorization, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");

  next();
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(expressValidator());
app.use(routes);

app.use((req, res, next) => {
  if (!req.dataOut) {
    next();
  } else {
    res.send({
      success: true,
      data: req.dataOut
    });
  }

});

app.use((req, res, next) => {
  next('api not found')
});

app.use((err, res, req, next) => {
  logger.info(`Error in req: ${JSON.stringify(err)}`);
  req.send({
    success: false,
    error: err
  })
});

sequelize.connect()
  .then(() => {
    app.listen(appConfig.server.port,
      appConfig.server.host,
      () => {
        logger.info(`server was started at port ${appConfig.server.port}`);
      });
  });


module.exports = app;
