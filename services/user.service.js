const db = require('../models')
  , logger = require('../configs/logger.config');

module.exports = {

  createTable: function () {
    return db.user.sync({force: true})
      .then(() => {
        logger.info('table User created');
      });
  },

  addUser: function (email, hash) {
    return this.findUserByEmail(email)
      .then((user) => {
        if (user == null) {
          return db.user.create({
            email: email,
            hash: hash
          })
        }
      });
  },

  findAllUsers: function () {
    return db.user.findAll({
      attributes: ['id', 'email', 'hash', 'tokenGenerateTime']
    })
  },

  findUserByEmail: function (email) {
    return db.user.findOne({
      where: {email: email},
      attributes: ['id', 'email', 'hash', 'tokenGenerateTime']
    }).then((user) => {
      if (user) {
        return user.dataValues
      }
    })
  },

  findUserById: function (id) {
    return db.user.findById(id).then(user => {
      if (user) {
        return user.dataValues
      }
    })
  },

  setTokenGenerateTime: function (id) {
    return db.user.update({
      tokenGenerateTime: Date.now()
    }, {
      where: {id: id}
    })
  }


}
