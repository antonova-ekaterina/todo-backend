const devConfig = require('../configs/environment/dev.env.config')
  , prodConfig = require('../configs/environment/prod.env.config');

function setEnvConfig() {
  switch (process.env.NODE_ENV) {
    case 'development':
      return devConfig;
    case 'production':
      return prodConfig;
    default :
      return devConfig
  }
}

module.exports = setEnvConfig();
