const logger = require('../configs/logger.config')
  , errors = require('../errors');

module.exports = {

  isError: function (validationErr, failingFunction, successFunction) {
    !validationErr.isEmpty() ? failingFunction(validationErr.array()[0].msg) : successFunction();
  },

  ifError: function (error, nextFun) {
    logger.error(error);
    nextFun(errors.common);
  }
}
