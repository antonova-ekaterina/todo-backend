const userService = require('./user.service')
  , noteService = require('./note.service');

module.exports = {
  user: userService,
  note: noteService
}
