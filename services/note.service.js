const db = require('../models')
  , userService = require('./user.service');

module.exports = {

  createTable: function () {
    return db.note.sync({force: true})
  },

  addNote: function (text, color, userEmail) {
    return db.note.create({
      text: text,
      color: color
    })
      .then((note) => {
        return userService.findUserByEmail(userEmail)
          .then((user) => {
            return note.setUser(user.id);
          })
          .then((note) => {
            return note.dataValues
          })
      })
  },

  getNoteByUserId: function (userId) {
    return db.note.findAll({
      where: {userId: userId},
      attributes: ['id', 'text', 'color']
    })
  },

  deleteNoteById: function (noteId) {
    return db.note.destroy({
      where: {id: noteId}
    })
  },

  updateNoteById: function (noteId, text, color) {
    return db.note.update({
      text: text,
      color: color
    }, {
      where: {id: noteId}
    })
  }
}
