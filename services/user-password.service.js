const bcrypt = require('bcryptjs')
  , logger = require('../configs/logger.config');

module.exports = {

  genHash: function (userPassword) {
    return bcrypt.hashSync(userPassword, bcrypt.genSaltSync(8));
  },

  compareHash: function (userPassword, dbHash, successFun, errFun) {
    bcrypt.compare(userPassword, dbHash, (error, result) => {
      logger.error(`error = ${error}`);
      result ? successFun() : errFun()
    })
  }
}
