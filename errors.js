module.exports = {

  user: {
    invalid_email: {
      code: 1001,
      description: 'invalid email'
    },
    invalid_password: {
      code: 1002,
      description: 'invalid password'
    },
    user_with_such_email_already_exist: {
      code: 1003,
      description: 'user with such email already exist'
    },
    no_such_user_found: {
      code: 1004,
      description: 'no such user found'
    },
    passwords_did_not_match: {
      code: 1005,
      description: 'passwords did not match'
    }
  },

  note: {
    invalid_text: {
      code: 2001,
      description: 'invalid text'
    },
    invalid_color: {
      code: 2002,
      description: 'invalid color'
    },
    invalid_id: {
      code: 2003,
      description: 'invalid note id'
    }
  },

  common: {
    code: 1000,
    description: 'unknown error'
  }

};
