let devConfig = {
  server: {
    port: 3000,
    host: '127.0.0.1'
  },
  db: {
    port: 5432,
    host: '127.0.0.1',
    name: 'todo',
    userName: 'postgres',
    password: '1'
  },
  morgan: {
    format: 'dev'
  },
  winston: {
    loggingLevel: 'debug'
  },
  token: {
    liveTime: 36000 // 36 sec
  }
};

module.exports = devConfig;
