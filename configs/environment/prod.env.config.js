let prodEnvConfig = {
  server: {
    port: 5000,
    host: '127.0.0.1'
  },
  db: {
    port: 5432,
    host: '127.0.0.1',
    name: 'todo',
    userName: 'postgres',
    password: '1'
  },
  morgan: {
    format: (tokens, req, res) => {
      return [
        tokens.method(req, res),
        tokens.url(req, res),
        tokens.status(req, res)
      ].join(' ')
    }
  },
  winston: {
    loggingLevel: 'info'
  },
  token: {
    liveTime: 172800000 // 48 hours
  }

};

module.exports = prodEnvConfig;
