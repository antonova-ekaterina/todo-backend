const winston = require('winston')
  , appConfig = require('../services/app-config.services');

// error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5

const logger = new winston.Logger({
  timestamp: () => {
    return (new Date()).toISOString();
  },
  transports: [
    new (winston.transports.Console)({
      level: appConfig.winston.loggingLevel,
    }),
    new (winston.transports.File)({
      name: 'info-file',
      filename: 'log-info.log',
      level: 'info'
    }),
    new (winston.transports.File)({
      name: 'error-file',
      filename: 'log-error.log',
      level: 'error'
    })
  ]
});

module.exports = logger;
