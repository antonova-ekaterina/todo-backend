const passport = require("passport")
  , passportJWT = require("passport-jwt")
  , _ = require("lodash")
  , ExtractJwt = passportJWT.ExtractJwt
  , JwtStrategy = passportJWT.Strategy
  , services = require('../services')
  , SECRET_WORD = require('../const').SECRET_WORD
  , logger = require('./logger.config')
  , appConfig = require('../services/app-config.services');

let jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
jwtOptions.secretOrKey = SECRET_WORD;

let strategy = new JwtStrategy(jwtOptions, (jwt_payload, next) => {
  services.user.findUserById(jwt_payload.id)
    .then((user) => {
      logger.debug(`user = ${user}`);
      logger.debug(`user.tokenGenerateTime = ${user.tokenGenerateTime}`);
      logger.debug(`Date.now() - user.tokenGenerateTime = ${Date.now() - user.tokenGenerateTime}`);

        if (user && Date.now() - user.tokenGenerateTime < appConfig.token.liveTime) {
          next(null, user);
        } else {
          next(null, false);
        }
    })
});

module.exports = () => {
  passport.use(strategy);
};
