const appConfig = require('../services/app-config.services')
  , Sequelize = require('sequelize')
  , logger = require('./logger.config');

let userName = appConfig.db.userName
  , name = appConfig.db.name;

const sequelize = new Sequelize(appConfig.db.name, appConfig.db.userName, appConfig.db.password, {
  host: appConfig.db.host,
  dialect: 'postgres',
  operatorsAliases: false,
});

exports.connect = () =>
  sequelize
    .authenticate()
    .then(() => {
      logger.info('Connection has been established successfully.');
    })
    .catch(err => {
      logger.error('Unable to connect to the database:', err);
    });

module.exports.connection = sequelize;
