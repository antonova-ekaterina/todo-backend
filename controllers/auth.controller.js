const SECRET_WORD = require('../const').SECRET_WORD
  , jwt = require('jsonwebtoken')
  , services = require('../services')
  , {validationResult} = require('express-validator/check')
  , errors = require('../errors')
  , validationService = require('../services/validation.service')
  , userPasswordService = require('../services/user-password.service')
  , logger = require('../configs/logger.config');
const userServise = require('../services/user.service');

module.exports = {

  login: (req, res, next) => {
    validationService.isError(validationResult(req), next, () => {
      services.user.findUserByEmail(req.body.email)
        .then((user) => {
          if (!user) {
            next(errors.user.no_such_user_found);
          } else {
            logger.debug(`user.hash = ${user.hash}`);
            userPasswordService.compareHash(req.body.password, user.hash, () => {
              let payload = {id: user.id};
              let token = jwt.sign(payload, SECRET_WORD);
              req.dataOut = {token: token};
              userServise.setTokenGenerateTime(user.id);
              next();
            }, () => {
              next(errors.user.passwords_did_not_match);
            });
          }
        })
        .catch((error) => {
          validationService.ifError(error, next);
        });
    });
  }
}
