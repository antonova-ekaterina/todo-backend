const services = require('../services')
  , {validationResult} = require('express-validator/check')
  , errors = require('../errors')
  , validationService = require('../services/validation.service')
  , userPasswordService = require('../services/user-password.service');

module.exports = {

  addUser: (req, res, next) => {
    validationService.isError(validationResult(req), next, () => {
      let hash = userPasswordService.genHash(req.body.password);
      services.user.addUser(req.body.email, hash)
        .then((user) => {
          if (user) {
            req.dataOut = {email: user.email};
            next()
          } else {
            next(errors.user.user_with_such_email_already_exist)
          }
        })
        .catch((error) => {
          validationService.ifError(error, next);
        });
    });
  },

  getAllUsers: (req, res, next) => {
    services.user.findAllUsers()
      .then((users) => {
        req.dataOut = users;
        next()
      })
      .catch((error) => {
        validationService.ifError(error, next);
      });
  }
}
