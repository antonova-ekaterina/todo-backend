const services = require('../services')
  , {validationResult} = require('express-validator/check')
  , validationService = require('../services/validation.service');

module.exports = {

  addNote: (req, res, next) => {
    validationService.isError(validationResult(req), next, () => {
      services.note.addNote(req.body.text, req.body.color, req.user.email)
        .then((note) => {
          req.dataOut = {
            id: note.id,
            text: note.text,
            color: note.color
          };
          next();
        })
        .catch((error) => {
          validationService.ifError(error, next);
        });
      ;
    });
  },

  getNote: (req, res, next) => {
    services.note.getNoteByUserId(req.user.id)
      .then((notes) => {
        req.dataOut = notes;
        next();
      })
      .catch((error) => {
        validationService.ifError(error, next);
      });
  },

  deleteNote: (req, res, next) => {
    validationService.isError(validationResult(req), next, () => {
      services.note.deleteNoteById(req.query.noteId)
        .then(() => {
          req.dataOut = 'deleted';
          next();
        })
        .catch((error) => {
          validationService.ifError(error, next);
        });
      ;
    });
  },

  changeNote: (req, res, next) => {
    validationService.isError(validationResult(req), next, () => {
      services.note.updateNoteById(req.query.noteId, req.body.text, req.body.color)
        .then(() => {
          req.dataOut = 'changed';
          next();
        })
        .catch((error) => {
          validationService.ifError(error, next);
        });
    });
  }
}
