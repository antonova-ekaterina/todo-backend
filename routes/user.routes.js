const {check} = require('express-validator/check')
  , {sanitize} = require('express-validator/filter')
  , errors = require('../errors')
  , userController = require('../controllers/user.controller')
  , router = require('express').Router();


router.post('/user', [
    check('email', errors.user.invalid_email).isEmail(),
    check('password', errors.user.invalid_password).not().isEmpty().isLength({max: 30}),
    sanitize('password').toString()
  ],
  userController.addUser
);

router.get('/user',
  userController.getAllUsers
);

module.exports = router;