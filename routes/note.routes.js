const passport = require("passport")
  , jwt = require('jsonwebtoken')
  , {check} = require('express-validator/check')
  , errors = require('../errors')
  , noteController = require('../controllers/note.controller')
  , router = require('express').Router();

router.get("/note", function (req, res, next) {
  console.log("body ", req.get('Authorization'));
  next();
});

router.post("/note", [
    check('text', errors.note.invalid_text).not().isEmpty().isLength({max: 30000}),
    check('color', errors.note.invalid_color).not().isEmpty().isLength({max: 20}),
  ], passport.authenticate('jwt', {session: false}),
  noteController.addNote
);

router.get('/note',
  passport.authenticate('jwt', {session: false}),
  noteController.getNote
);

router.delete('/note', passport.authenticate('jwt', {session: false}),
  noteController.deleteNote
);

router.put('/note', passport.authenticate('jwt', {session: false}),
  noteController.changeNote
)

module.exports = router;