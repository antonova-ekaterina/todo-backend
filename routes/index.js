const userRoutes = require('./user.routes')
  , authorizationRoutes = require('./auth')
  , noteRoutes = require('./note.routes')
  , loggingRouters = require('./logging.routes')
  , router = require('express').Router();

router.use(authorizationRoutes);
router.use(loggingRouters);
router.use(userRoutes);
router.use(noteRoutes);

module.exports = router;