const morgan = require('morgan')
  , appConfig = require('../services/app-config.services')
  , router = require('express').Router();


router.use(morgan(appConfig.morgan.format, {
  skip: (req, res) => {
    return res.statusCode < 400
  },
  stream: process.stderr
}));

router.use(morgan(appConfig.morgan.format, {
  skip: (req, res) => {
    return res.statusCode >= 400
  },
  stream: process.stdout
}));

module.exports = router;