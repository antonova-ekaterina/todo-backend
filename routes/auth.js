const passport = require("passport")
  , {check} = require('express-validator/check')
  , {sanitize} = require('express-validator/filter')
  , errors = require('../errors')
  , auth = require('../controllers/auth.controller');

const router = require('express').Router();

router.use(passport.initialize());

router.post("/login", [
    check('email', errors.user.invalid_email).isEmail(),
    check('password', errors.user.invalid_password).not().isEmpty().isLength({max: 30}),
    sanitize('password').toString()
  ],
  auth.login
);

module.exports = router;