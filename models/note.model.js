const sequelize = require('../configs/sequelize.config').connection
  , Sequelize = require('sequelize');

module.exports = Note = sequelize.define('note', {
  text: Sequelize.TEXT,
  color: Sequelize.STRING
});
