const userModel = require('./user.model')
  , noteModel = require('./note.model');

module.exports = {
  user: userModel,
  note: noteModel
};

noteModel.belongsTo(userModel);
