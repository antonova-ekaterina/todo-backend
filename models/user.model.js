const Sequelize = require('sequelize')
  , sequelize = require('../configs/sequelize.config').connection;

const User = sequelize.define('user', {
  email: {type: Sequelize.STRING, unique: true},
  hash: {type: Sequelize.STRING},
  tokenGenerateTime: {type: Sequelize.DATE}
});
module.exports = User;
