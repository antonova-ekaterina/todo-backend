const sequelize = require('../configs/sequelize.config')
  , services = require('../services');

sequelize.connect()
  .then(() => {
    return services.user.createTable();
  })
  .then(() => {
    services.note.createTable();
  });
